﻿using HoneyWell.OneDAT.Commons.Model;
using HoneyWell.OneDAT.SubModule.API.Application.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.SubModule.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class SubModuleController : ControllerBase
    {
        private readonly ILogger<SubModuleController> _logger;

        private readonly IConfiguration _config;

        public SubModuleController(ILogger<SubModuleController> logger, IConfiguration config)
        {
            _logger = logger;
            _config = config;
        }

        [HttpPost]
        [Route("/")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<OperationStatus>> PostAsync([FromBody] DataRequest dataRequest)
        {
            SubModuleService _rrs = new SubModuleService(_config);

            OperationStatus operationstatus = await _rrs.ProcessRequest(dataRequest);

            return Ok(operationstatus);
        }
    }
}

