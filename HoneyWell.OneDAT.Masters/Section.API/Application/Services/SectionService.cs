﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.Extensions.Configuration;
using Microsoft.Rest.TransientFaultHandling;

namespace HoneyWell.OneDAT.Section.API.Application.Services
{
    public class SectionService : OneDATService
    {
        Operations operations;

        public SectionService(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
        }

        public override async Task<OperationStatus> Create(DataRequest dataRequest)
        {
            return await operations.Create(dataRequest);
        }

        public override async Task<OperationStatus> Read(DataRequest dataRequest)
        {
            return await operations.Read(dataRequest);
        }

        public override async Task<OperationStatus> Update(DataRequest dataRequest)
        {
            return await operations.Update(dataRequest);
        }

        public override async Task<OperationStatus> Delete(DataRequest dataRequest)
        {
            return await operations.Delete(dataRequest);
        }

    }

}
