﻿using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.Module.API.Application.Services
{
    public class ModuleService : OneDATService
    {
        readonly Operations operations;

        public ModuleService(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
        }

        public override async Task<OperationStatus> Create(DataRequest dataRequest)
        {
            return await operations.Create(dataRequest);
        }

        public override async Task<OperationStatus> Read(DataRequest dataRequest)
        {
            return await operations.Read(dataRequest);
        }

        public override async Task<OperationStatus> Read(DataRequest dataRequest, string sqlQuery)
        {
            return await operations.Read(dataRequest, sqlQuery);
        }

        public override async Task<OperationStatus> Update(DataRequest dataRequest)
        {
            return await operations.Update(dataRequest);
        }

        public override async Task<OperationStatus> Delete(DataRequest dataRequest)
        {
            return await operations.Delete(dataRequest);
        }

    }

}
