﻿using HoneyWell.OneDAT.Commons.Interfaces;

namespace HoneyWell.OneDAT.Module.API.Application.Queries
{
    public class Query : IQuery
    {
        private string _Query;

        public string GetQuery()
        {
            _Query = "";

            return _Query;
        }

        public string GetQuery(string QueryName)
        {
            switch (QueryName)
            {
                case "SubModuleQuery":
                    _Query = "select tm.TECH_ID_SQ, tm.TECH_NAME, mm.MODULE_ID, mm.MODULE_NAME, mm.MODULE_DESC, sbm.SUB_MODULE_ID, sbm.SUB_MODULE_NAME, " +
                            "tsm.UPDATED_BY_USER_ID, tsm.UPDATED_ON_DT " +
                            "from sc_template.TECH_SUB_MODULE tsm " +
                            "inner join sc_template.SUB_MODULE_MASTER sbm on tsm.SUB_MODULE_ID = sbm.SUB_MODULE_ID " +
                            "inner join sc_template.MODULE_MASTER mm on tsm.MODULE_ID = mm.MODULE_ID " +
                            "inner join sc_template.Tech_Master tm on tsm.TECH_ID = tm.TECH_ID_SQ ";
                    break;
                case "ModuleDtlQuery":
                    _Query = "select mm.MODULE_ID, mm.MODULE_NAME, mm.MODULE_DESC, sbm.SUB_MODULE_ID, sbm.SUB_MODULE_NAME, tmp.TEMPLATE_ID, " +
                            "tmp.VERSION, tmp.ACTIVE_IND, tmp.IS_TEMPLATE_DRIVEN, tsm.UPDATED_BY_USER_ID, tsm.UPDATED_ON_DT " +
                            "from sc_template.TECH_SUB_MODULE tsm " +
                            "inner join sc_template.SUB_MODULE_MASTER sbm on tsm.SUB_MODULE_ID = sbm.SUB_MODULE_ID " +
                            "inner join sc_template.MODULE_MASTER mm on tsm.MODULE_ID = mm.MODULE_ID " +
                            "inner join sc_template.Tech_Master tm on tsm.TECH_ID = tm.TECH_ID_SQ " +
                            "inner join sc_template.TEMPLATE_MASTER tmp on tmp.TECH_ID = tm.TECH_ID_SQ and tmp.MODULE_ID = mm.MODULE_ID " +
                            "and tmp.SUB_MODULE_ID = sbm.SUB_MODULE_ID ";
                    break; 
                default:
                    break;
            }

            return _Query;
        }
    }
}
