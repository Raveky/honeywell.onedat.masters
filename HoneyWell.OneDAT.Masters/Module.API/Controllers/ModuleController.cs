﻿using HoneyWell.OneDAT.Commons.Model;
using HoneyWell.OneDAT.Module.API.Application.Queries;
using HoneyWell.OneDAT.Module.API.Application.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.Module.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ModuleController : ControllerBase
    {
        private readonly ILogger<ModuleController> _logger;

        private readonly IConfiguration _config;

        public ModuleController(ILogger<ModuleController> logger, IConfiguration config)
        {
            _logger = logger;

            _config = config;
        }

        [HttpPost]
        [Route("/")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<OperationStatus>> PostAsync([FromBody] DataRequest dataRequest)
        {
            ModuleService _rrs = new ModuleService(_config);

            Query _query = new Query();
            OperationStatus operationstatus = await _rrs.Read(dataRequest, _query.GetQuery("SubModuleQuery"));

            return Ok(operationstatus);
        }

        [HttpPost]
        [Route("/GetModuleDtl")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<OperationStatus>> GetModuleDtlAsync([FromBody] DataRequest dataRequest)
        {
            ModuleService _rrs = new ModuleService(_config);

            Query _query = new Query();
            OperationStatus operationstatus = await _rrs.Read(dataRequest, _query.GetQuery("ModuleDtlQuery"));

            return Ok(operationstatus);
        }


        //[HttpGet]
        //[Route("/")]
        //[ProducesResponseType(StatusCodes.Status200OK)]
        //[ProducesResponseType(StatusCodes.Status500InternalServerError)]
        //public async Task<ActionResult<OperationStatus>> GetAsync([FromBody] DataRequest dataRequest)
        //{
        //    ModuleService _rrs = new ModuleService(_config);
        //    Query _query = new Query();
        //    OperationStatus operationstatus = await _rrs.Read(dataRequest, _query.GetQuery());

        //    return Ok(operationstatus);
        //}
    }
}
