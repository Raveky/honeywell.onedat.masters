﻿using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Infrastructure.Validations;
using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.CatalystSize.API.Application.Validators
{
    public class BussinessValidator : OneDATService, IValidator
    {
        readonly Operations operations;
        readonly OperationStatus operationStatus;
        readonly CommonValidations commonValidations;

        public BussinessValidator(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
            operationStatus = new OperationStatus();
            commonValidations = new CommonValidations(configuration);
        }
        public async Task<(bool status, string message)> Validate(DataRequest dataRequest)
        {
            if (dataRequest.Messages[0].fields.Count > 0)
            {
                foreach (var data in dataRequest.Messages[0].fields)
                {
                    if (data.field_name == "catalyst_size_cd")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Catalyst Size Name");
                        }

                        bool isDuplicateValue = await commonValidations.ChecksDuplicateRecord(data, dataRequest);

                        if (isDuplicateValue)
                        {
                            return (false, "Duplicate Catalyst Size");
                        }
                    }

                    if (data.field_name == "active_ind")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Catalyst Size Status");
                        }
                        else if (!(data.field_value.ToLower() == "y" || data.field_value.ToLower() == "n"))
                        {
                            return (false, "Invalid value of Status");
                        }
                    }
                }
            }
            return (true, "Valid Data");
        }
        public async Task<(bool status, string message)> ValidateRead(DataRequest dataRequest)
        {
            if (dataRequest.Messages[0].filter.Count > 0)
            {
                foreach (var data in dataRequest.Messages[0].filter)
                {
                    if (data.field_name == "catalyst_size_cd")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Catalyst Size Name");
                        }
                       
                    }

                    if (data.field_name == "active_ind")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Catalyst Size Status");
                        }
                        else if (!(data.field_value.ToLower() == "y" || data.field_value.ToLower() == "n"))
                        {
                            return (false, "Invalid value of Status");
                        }
                    }

                }

            }
            return (true, "Valid Data");
        }

    }
}

