﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HoneyWell.OneDAT.CatalystSize.API.Application.Validators;
using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.Extensions.Configuration;
using Microsoft.Rest.TransientFaultHandling;

namespace HoneyWell.OneDAT.CatalystSize.API.Application.Services
{
    public class CatalysSizeService : OneDATService
    {
        Operations operations;
        BussinessValidator bussinessValidator;
        OperationStatus objResult = new OperationStatus();
        public CatalysSizeService(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
            bussinessValidator = new BussinessValidator(configuration);
        }

        public override async Task<OperationStatus> Create(DataRequest dataRequest)
        {
            try
            {
                var insertResult = await bussinessValidator.Validate(dataRequest);

                if (!insertResult.status)
                {
                    objResult.Status = "Fail";
                    objResult.Message = insertResult.message.ToString();
                    return objResult;
                }

                return await operations.Create(dataRequest);

            }
            catch (Exception ex)
            {
                objResult.Status = "Fail";
                objResult.Message = "Error :" + ex.ToString();
                return objResult;
            }
        }
        public override async Task<OperationStatus> Read(DataRequest dataRequest)
        {
            try
            {
                return await operations.Read(dataRequest);
            }
            catch (Exception ex)
            {
                objResult.Status = "Fail";
                objResult.Message = "Error :" + ex.ToString();
                return objResult;
            }
        }
    

        public override async Task<OperationStatus> Update(DataRequest dataRequest)
        {
            //validate logic goes here

            return await operations.Update(dataRequest);
        }

        public override async Task<OperationStatus> Delete(DataRequest dataRequest)
        {
            //validate logic goes here

            return await operations.Delete(dataRequest);
        }

    }

}
