﻿using HoneyWell.OneDAT.Commons.Model;
using HoneyWell.OneDAT.Template.API.Application.Queries;
using HoneyWell.OneDAT.Template.API.Application.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.Template.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class TemplateController : ControllerBase
    {
        private readonly ILogger<TemplateController> _logger;

        private readonly IConfiguration _config;

        public TemplateController(ILogger<TemplateController> logger, IConfiguration config)
        {
            _logger = logger;

            _config = config;
        }

        [HttpPost]
        [Route("/")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<OperationStatus>> PostAsync([FromBody] DataRequest dataRequest)
        {
            TemplateService _rrs = new TemplateService(_config);

            OperationStatus operationstatus = await _rrs.ProcessRequest(dataRequest);

            return Ok(operationstatus);
        }

        [HttpPost]
        [Route("/GetTemplateData")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<OperationStatus>> GetTemplateDataAsync([FromBody] DataRequest dataRequest)
        {
            TemplateService _rrs = new TemplateService(_config);
            Query _query = new Query();
            OperationStatus operationstatus = await _rrs.Read(dataRequest, _query.GetQuery());

            return Ok(operationstatus);
        }
    }
}
