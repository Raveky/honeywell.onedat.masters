﻿using HoneyWell.OneDAT.Commons.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.Template.API.Application.Queries
{
    public class Query : IQuery
    {
        private string _Query;

        public string GetQuery()
        {
            _Query = "select * from sc_template.TEMPLATE_MASTER " +
                     "where TEMPLATE_DATA  is not null";

            return _Query;
        }

        public string GetQuery(string QueryName)
        {
            switch (QueryName)
            {
                case "CheckTemplateRecord":
                    _Query = "select count(1) rcount from sc_template.TEMPLATE_MASTER " +
                             "where tech_id = {0} and module_id = {1} and sub_module_id = {2}";
                    break;
                case "UpdateQuery":
                    _Query = "update sc_template.TEMPLATE_MASTER set ACTIVE_IND ='N' " +
                             "where TECH_ID = {0} and MODULE_ID = {1} and SUB_MODULE_ID = {2}";
                    break;
                default:
                    break;
            }
            return _Query;
        }
    }
}
