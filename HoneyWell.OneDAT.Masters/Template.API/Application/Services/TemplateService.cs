﻿using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Model;
using HoneyWell.OneDAT.Template.API.Application.Queries;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.Template.API.Application.Services
{
    public class TemplateService : OneDATService
    {
        readonly Operations operations;

        public TemplateService(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
        }

        public override async Task<OperationStatus> Create(DataRequest dataRequest)
        {
            return await operations.Create(dataRequest);
        }

        private async Task<bool> CheckAndUpdateTemplateRecord(DataRequest dataRequest)
        {
            Query query = new Query();

            int techID = 0;
            int moduleID = 0;
            int submoduleID = 0;

            foreach (var message in dataRequest.Messages)
            {
                foreach (var field in message.fields)
                {
                    switch (field.field_name)
                    {
                        case "tech_id":
                            techID = Convert.ToInt32(field.field_value);
                            break;
                        case "module_id":
                            moduleID = Convert.ToInt32(field.field_value);
                            break;
                        case "sub_module_id":
                            submoduleID = Convert.ToInt32(field.field_value);
                            break;
                        default:
                            break;
                    }
                }
            }

            if (moduleID != 0 && submoduleID != 0)
            {
                string finalQuery = query.GetQuery("CheckTemplateRecord");

                finalQuery = String.Format(finalQuery, techID, moduleID, submoduleID);

                string count = await operations.ExecuteScalarQuery(finalQuery);

                if (Convert.ToInt32(count) >= 1)
                {
                    string updateQuery = query.GetQuery("UpdateQuery");
                    updateQuery = string.Format(updateQuery, techID, moduleID, submoduleID);
                    await operations.ExecuteDbQuery(updateQuery);
                }
            }

            return true;
        }

        public override async Task<OperationStatus> Read(DataRequest dataRequest)
        {
            return await operations.Read(dataRequest);
        }

        public override async Task<OperationStatus> Update(DataRequest dataRequest)
        {
            bool flag = await CheckAndUpdateTemplateRecord(dataRequest);

            return await operations.Update(dataRequest);
        }

        public override async Task<OperationStatus> Delete(DataRequest dataRequest)
        {
            return await operations.Delete(dataRequest);
        }

    }

}
