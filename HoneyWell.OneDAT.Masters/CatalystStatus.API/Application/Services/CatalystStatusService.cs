﻿using HoneyWell.OneDAT.CatalystStatus.API.Application.Queries;
using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.CatalystStatus.API.Application.Services
{
    public class CatalystStatusService : OneDATService
    {
        Operations operations;
        OperationStatus objResult;
        public CatalystStatusService(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
            objResult = new OperationStatus();
        }

        public override async Task<OperationStatus> Create(DataRequest dataRequest)
        {
            return await operations.Create(dataRequest);
        }

        public override async Task<OperationStatus> Read(DataRequest dataRequest)
        {
            return await operations.Read(dataRequest);
        }

        public override async Task<OperationStatus> Read(DataRequest dataRequest, string sqlQuery)
        {

            Query qry = new Query();
            sqlQuery = qry.GetQuery();
            return await operations.Read(dataRequest, sqlQuery);
        }

        public override async Task<OperationStatus> Update(DataRequest dataRequest)
        {
            //validate logic goes here

            return await operations.Update(dataRequest);
        }

        public override async Task<OperationStatus> Delete(DataRequest dataRequest)
        {
            //validate logic goes here

            return await operations.Delete(dataRequest);
        }

    }

}
