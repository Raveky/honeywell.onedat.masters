﻿using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Model;
using HoneyWell.OneDAT.Table.API.Application.Queries;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.Table.API.Application.Services
{
    public class TableService : OneDATService
    {
        readonly Operations operations;

        public TableService(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
        }

        public override async Task<OperationStatus> Create(DataRequest dataRequest)
        {
            return await operations.Create(dataRequest);
        }

        public override async Task<OperationStatus> Read(DataRequest dataRequest)
        {
            Query query = new Query();
            return await operations.Read(dataRequest, query.GetQuery());
        }

        public override async Task<OperationStatus> Update(DataRequest dataRequest)
        {
            return await operations.Update(dataRequest);
        }

        public override async Task<OperationStatus> Delete(DataRequest dataRequest)
        {
            return await operations.Delete(dataRequest);
        }

    }

}
