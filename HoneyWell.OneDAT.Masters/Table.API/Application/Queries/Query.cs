﻿using HoneyWell.OneDAT.Commons.Interfaces;

namespace HoneyWell.OneDAT.Table.API.Application.Queries
{
    public class Query : IQuery
    {
        private string _Query;

        public string GetQuery()
        {
            _Query = "select tbl.table_schema + '.' + tbl.table_name as tablename, " +
                              "prop.value as title " +
                              "FROM information_schema.tables tbl " +
                              "LEFT JOIN sys.extended_properties prop " +
                              "    ON prop.major_id = object_id(tbl.table_schema + '.' + tbl.table_name) " +
                              "    AND prop.minor_id = 0 " +
                              "    AND prop.name = 'MS_Description' " +
                              "WHERE tbl.table_type = 'base table' " +
                              "and tbl.TABLE_SCHEMA like 'sc_%'";

            return _Query;
        }

        public string GetQuery(string queryName)
        {
            throw new System.NotImplementedException();
        }
    }
}
