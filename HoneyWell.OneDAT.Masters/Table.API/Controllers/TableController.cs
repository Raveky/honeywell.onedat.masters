﻿using HoneyWell.OneDAT.Commons.Model;
using HoneyWell.OneDAT.Table.API.Application.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.Table.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class TableController : ControllerBase
    {
        private readonly ILogger<TableController> _logger;

        private readonly IConfiguration _config;

        public TableController(ILogger<TableController> logger, IConfiguration config)
        {
            _logger = logger;

            _config = config;
        }
      
        [HttpPost]
        [Route("/")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<OperationStatus>> GetAsync([FromBody] DataRequest dataRequest)
        {
            TableService _rrs = new TableService(_config);

            OperationStatus operationstatus = await _rrs.ProcessRequest(dataRequest);

            return Ok(operationstatus);
        }
    }
}
