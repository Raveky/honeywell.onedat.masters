﻿using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Infrastructure.Validations;
using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.Commons.Model;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.CatalystState.API.Application.Validators
{
    public class BussinessValidator : OneDATService, IValidator
    {
        readonly SqlConnectionStringBuilder connBuilder = new SqlConnectionStringBuilder();
        readonly Operations operations;
        readonly OperationStatus operationStatus;
        readonly CommonValidations commonValidations;

        public BussinessValidator(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
            operationStatus = new OperationStatus();
            commonValidations = new CommonValidations(configuration);
            connBuilder.ConnectionString = configuration.GetConnectionString("DefaultConnection");
        }
        public async Task<(bool status, string message)> Validate(DataRequest dataRequest)
        {
            if (dataRequest.Messages[0].fields.Count > 0)
            {
                foreach (var data in dataRequest.Messages[0].fields)
                {
                    if (data.field_name == "catalyst_state_nm")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Catalyst State");
                        }
                        else if (data.field_value.Length > 30)
                        {
                            return (false, "Field length should not be greather that 30");
                        }
                        
                        bool isDuplicateValue = await commonValidations.ChecksDuplicateRecord(data, dataRequest);

                        if (isDuplicateValue)
                        {
                            return (false, "Duplicate Catalyst State");
                        }
                    }

                    if (data.field_name == "sulfiding_ind")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Sulfiding / Reduction Indicator");
                        }
                        if (!(data.field_value.ToLower() == "y" || data.field_value.ToLower() == "n"))
                        {
                            return (false, "Invalid Value of Sulfiding / Reduction Indicator");
                        }
                    }

                    if (data.field_name == "active_ind")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Catalyst State Status");
                        }
                        else if (!(data.field_value.ToLower() == "y" || data.field_value.ToLower() == "n"))
                        {
                            return (false, "Invalid value of Status");
                        }
                    }

                    if (data.field_name == "catalyst_state_cd")
                    {
                        data.field_value = await CreateCatalystStateCD(dataRequest.Messages[0].table_name);
                    }

                }
            }
            return (true, "Valid Data");
        }

        public async Task<(bool status, string message)> ValidateRead(DataRequest dataRequest)
        {
            if (dataRequest.Messages[0].filter.Count > 0)
            {
                foreach (var data in dataRequest.Messages[0].filter)
                {
                    if (data.field_name == "catalyst_state_nm")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Catalyst State");
                        }
                        else if (data.field_value.Length > 30)
                        {
                            return (false, "Field length should not be greather that 30");
                        }

                       
                    }

                    if (data.field_name == "sulfiding_ind")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Sulfiding / Reduction Indicator");
                        }
                        else if (!(data.field_value.ToLower() == "y" || data.field_value.ToLower() == "n"))
                        {
                            return (false, "Invalid Sulfiding / Reduction Indicator");
                        }
                    }

                    if (data.field_name == "active_ind")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Catalyst State Status");
                        }
                        else if (!(data.field_value.ToLower() == "y" || data.field_value.ToLower() == "n"))
                        {
                            return (false, "Invalid value of Status");
                        }
                    }
                }
            }
            return (true, "Valid Data");
        }

        public async Task<string> CreateCatalystStateCD(string seqName)
        {
            using (SqlConnection connection = new SqlConnection(connBuilder.ConnectionString))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    await connection.OpenAsync();
                }

                string returnValue = null;
                int numberStateCd = Helpers.GetMaxValueofPrimaryKey(connection, "catalyst_state_id_sq", seqName);

                numberStateCd = numberStateCd + 1;

                if (numberStateCd <= 9)
                {
                    returnValue = "CS00" + numberStateCd;
                }

                if (numberStateCd > 9 && numberStateCd <= 99)
                {
                    returnValue = "CS0" + numberStateCd;
                }

                if (numberStateCd > 99)
                {
                    returnValue = "CS" + numberStateCd;
                }
                return returnValue;
            }
        }

    }
}
