﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.Extensions.Configuration;
using Microsoft.Rest.TransientFaultHandling;
using HoneyWell.OneDAT.CatalystState.API.Application.Validators;


namespace HoneyWell.OneDAT.CatalystState.API.Application.Services
{
    public class CatalystStateService : OneDATService
    {
        Operations operations;
        BussinessValidator bussinessValidator;
        OperationStatus objResult = new OperationStatus();

        public CatalystStateService(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
            bussinessValidator = new BussinessValidator(configuration);
        }

        public override async Task<OperationStatus> Create(DataRequest dataRequest)
        {
            try
            {
                var insertResult = await bussinessValidator.Validate(dataRequest);

                if (!insertResult.status)
                {
                    objResult.Status = "Fail";
                    objResult.Message = insertResult.message.ToString();
                    return objResult;
                }

                return await operations.Create(dataRequest);

            }
            catch (Exception ex)
            {
                objResult.Status = "Fail";
                objResult.Message = "Error :" + ex.ToString();
                return objResult;
            }

        }

        public override async Task<OperationStatus> Read(DataRequest dataRequest)
        {
            try
            {
                return await operations.Read(dataRequest);
            }
            catch (Exception ex)
            {
                objResult.Status = "Fail";
                objResult.Message = "Error :" + ex.ToString();
                return objResult;
            }
        }

        public override async Task<OperationStatus> Update(DataRequest dataRequest)
        {
            //validate logic goes here

            return await operations.Update(dataRequest);
        }

        public override async Task<OperationStatus> Delete(DataRequest dataRequest)
        {
            //validate logic goes here

            return await operations.Delete(dataRequest);
        }

    }

}
