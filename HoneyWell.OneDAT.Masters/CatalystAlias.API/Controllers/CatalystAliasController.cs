﻿using HoneyWell.OneDAT.CatalystAlias.API.Application.Services;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HoneyWell.OneDAT.CatalystAlias.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CatalystAliasController : ControllerBase
    {
        private readonly ILogger<CatalystAliasController> _logger;

        private readonly IConfiguration _config;

        public CatalystAliasController(ILogger<CatalystAliasController> logger , IConfiguration config)
        {
            _logger = logger;

            _config = config;             
        }

        [HttpPost]
        [Route("/")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<OperationStatus>> PostAsync([FromBody] DataRequest dataRequest)
        {
            CatalystAliasService _rrs = new CatalystAliasService(_config);

            OperationStatus operationstatus = await _rrs.ProcessRequest(dataRequest);

            return Ok(operationstatus);
        }
    }
}
