﻿using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.CatalystAlias.API.Application.Services
{
    public class CatalystAliasService : OneDATService
    {
        readonly Operations operations;

        public CatalystAliasService(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
        }

        public override async Task<OperationStatus> Create(DataRequest dataRequest)
        {
            //validate logic goes here
            //throw new ValidationException("Validation Failed");

            return await operations.Create(dataRequest);
        }

        public override async Task<OperationStatus> Read(DataRequest dataRequest)
        {
            //validate logic goes here

            return await operations.Read(dataRequest);
        }

        public override async Task<OperationStatus> Update(DataRequest dataRequest)
        {
            //validate logic goes here

            return await operations.Update(dataRequest);
        }

        public override async Task<OperationStatus> Delete(DataRequest dataRequest)
        {
            //validate logic goes here

            return await operations.Delete(dataRequest);
        }

    }

}
