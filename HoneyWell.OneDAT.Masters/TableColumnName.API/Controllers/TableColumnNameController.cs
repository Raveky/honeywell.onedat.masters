﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using HoneyWell.OneDAT.TableColumnName.API.Application.Services;

namespace HoneyWell.OneDAT.TableColumnName.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class TableColumnNameController : ControllerBase
    {
        private readonly ILogger<TableColumnNameController> _logger;

        private readonly IConfiguration _config;

        public TableColumnNameController(ILogger<TableColumnNameController> logger, IConfiguration config)
        {
            _logger = logger;

            _config = config;
        }

        [HttpPost]
        [Route("/")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<OperationStatus>> PostAsync([FromBody] DataRequest dataRequest)
        {
            TableColumnNameService _rrs = new TableColumnNameService(_config);

            OperationStatus operationstatus = await _rrs.ProcessRequest(dataRequest);

            return Ok(operationstatus);
        }
            
    }
}
