﻿using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Model;
using HoneyWell.OneDAT.TableColumnName.API.Application.Queries;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.TableColumnName.API.Application.Services
{
    public class TableColumnNameService : OneDATService
    {
        readonly Operations operations;

        public TableColumnNameService(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
        }

        public override async Task<OperationStatus> Create(DataRequest dataRequest)
        {
            return await operations.Create(dataRequest);
        }

        public override async Task<OperationStatus> Read(DataRequest dataRequest)
        {
            Query query = new Query();
            string finalquery = String.Format(query.GetQuery(), dataRequest.Messages[0].table_name);

            return await operations.Read(dataRequest, finalquery);
        }

        public override async Task<OperationStatus> Update(DataRequest dataRequest)
        {
            //validate logic goes here

            return await operations.Update(dataRequest);
        }

        public override async Task<OperationStatus> Delete(DataRequest dataRequest)
        {
            //validate logic goes here

            return await operations.Delete(dataRequest);
        }

    }

}
