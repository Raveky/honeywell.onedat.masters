﻿using HoneyWell.OneDAT.Commons.Interfaces;

namespace HoneyWell.OneDAT.TableColumnName.API.Application.Queries
{
    public class Query : IQuery
    {
        private string _Query;

        public string GetQuery()
        {
            _Query = "select lower(sc.name) [columnname], " +
                                "sep.value [title], " +
                                "sc.max_length [maxlength], " +
                                "t.name [datatype] " +
                            "from sys.tables st " +
                            "inner join sys.columns sc on st.object_id = sc.object_id " +
                            "inner join sys.schemas s  on st.schema_id = s.schema_id " +
                            "inner join sys.types t on sc.system_type_id = t.system_type_id " +
                            "left join sys.extended_properties sep on st.object_id = sep.major_id " +
                                  "and sc.column_id = sep.minor_id " +
                                  "and sep.name = 'MS_Description'" +
                            "where  s.name like 'sc_%' and " +
                            "st.name = '{0}'";

            return _Query;
        }

        public string GetQuery(string queryName)
        {
            throw new System.NotImplementedException();
        }
    }
}
