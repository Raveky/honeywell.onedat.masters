﻿using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Infrastructure.Validations;
using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.CatalystType.API.Application.Validators
{
    public class BussinessValidator : OneDATService, IValidator
    {
        Operations operations;
        OperationStatus operationStatus;
        CommonValidations commonValidations;

        public BussinessValidator(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
            operationStatus = new OperationStatus();
            commonValidations = new CommonValidations(configuration);
        }
        public async Task<(bool status, string message)> Validate(DataRequest dataRequest)
        {
            if (dataRequest.Messages[0].fields.Count > 0)
            {
                foreach (var data in dataRequest.Messages[0].fields)
                {
                    if (data.field_name == "catalyst_type_nm")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Catalyst Type Name");
                        }

                        bool isDuplicateValue = await commonValidations.ChecksDuplicateRecord(data, dataRequest);

                        if (isDuplicateValue)
                        {
                            return (false, "Duplicate Catalyst Type Name");
                        }
                    }

                    if (data.field_name == "catalyst_type_desc")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Catalyst Type Description");
                        }
                        else if ((!System.Text.RegularExpressions.Regex.IsMatch(data.field_value, @"^\w*([\s-_.]*\w*)*$")))
                        {
                            return (false, "Invalid Catalyst Type Description");
                        }

                        bool isDuplicateValue = await commonValidations.ChecksDuplicateRecord(data, dataRequest);

                        if (isDuplicateValue)
                        {
                            return (false, "Duplicate Catalyst Type Description");
                        }
                    }

                    if (data.field_name == "active_ind")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Catalyst Type Status");
                        }
                        else if (!(data.field_value.ToLower() == "y" || data.field_value.ToLower() == "n"))
                        {
                            return (false, "Invalid value of Status");
                        }
                    }
                }
            }
            return (true, "Valid Data");
        }

        public async Task<(bool status, string message)> ValidateRead(DataRequest dataRequest)
        {
            if (dataRequest.Messages[0].filter.Count > 0)
            {
                foreach (var data in dataRequest.Messages[0].filter)
                {
                    if (data.field_name == "catalyst_type_nm")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Catalyst Type Name");
                        }
                       
                    }

                    if (data.field_name == "catalyst_type_desc")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Catalyst Type Description");
                        }
                        else if ((!System.Text.RegularExpressions.Regex.IsMatch(data.field_value, @"^\w*([\s-_.]*\w*)*$")))
                        {
                            return (false, "Invalid Catalyst Type Description");
                        }
                    }

                    if (data.field_name == "active_ind")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Catalyst Type Status");
                        }
                        else if (!(data.field_value.ToLower() == "y" || data.field_value.ToLower() == "n"))
                        {
                            return (false, "Invalid value of Status");
                        }
                    }
                }
            }
            return (true, "Valid Data");
        }
    }
}
