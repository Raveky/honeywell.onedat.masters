﻿using HoneyWell.OneDAT.CatalystFamily.API.Application.Services;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HoneyWell.OneDAT.CatalystFamily.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CatalystFamilyController : ControllerBase
    {
        private readonly ILogger<CatalystFamilyController> _logger;

        private readonly IConfiguration _config;

        public CatalystFamilyController(ILogger<CatalystFamilyController> logger , IConfiguration config)
        {
            _logger = logger;

            _config = config;             
        }

        [HttpPost]
        [Route("/")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<OperationStatus>> PostAsync([FromBody] DataRequest dataRequest)
        {
            CatalysFamilyService _rrs = new CatalysFamilyService(_config);

            OperationStatus operationstatus = await _rrs.ProcessRequest(dataRequest);

            return Ok(operationstatus);
        }
        [HttpPost]
        [Route("/application")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<OperationStatus>> PostAsyncapplication([FromBody] DataRequest dataRequest)
        {
            CatalystApplicationService _rrs = new CatalystApplicationService(_config);
            string sqlQuery = "";
            OperationStatus operationstatus = await _rrs.Read(dataRequest, sqlQuery);

            return Ok(operationstatus);
        }
    }
}
