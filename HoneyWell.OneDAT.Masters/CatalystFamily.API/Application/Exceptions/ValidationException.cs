﻿using System;

namespace HoneyWell.OneDAT.CatalystFamily.API.Application.Exceptions
{
    public class ValidationException : Exception
    {
        public ValidationException()
        { }

        public ValidationException(string message)
            : base(message)
        { }

        public ValidationException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
