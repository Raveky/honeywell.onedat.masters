﻿using HoneyWell.OneDAT.CatalystFamily.API.Application.Queries;
using HoneyWell.OneDAT.CatalystFamily.API.Application.Validators;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.CatalystFamily.API.Application.Services
{
    public class CatalystApplicationService : OneDATService
    {
        readonly Operations operations;
        readonly BussinessValidator insertValidator;
        OperationStatus objResult = new OperationStatus();
        public CatalystApplicationService(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
            insertValidator = new BussinessValidator(configuration);
        }

        public override async Task<OperationStatus> Create(DataRequest dataRequest)
        {
            return await operations.Create(dataRequest);
        }
        public override async Task<OperationStatus> Read(DataRequest dataRequest)
        {
            return await operations.Read(dataRequest);
        }
        public override async Task<OperationStatus> Read(DataRequest dataRequest, string sqlQuery)
        {
            Query qry = new Query();
            sqlQuery = qry.GetQueryApplication();
            return await operations.Read(dataRequest, sqlQuery);
        }

        public override async Task<OperationStatus> Update(DataRequest dataRequest)
        {
            return await operations.Update(dataRequest);
        }

        public override async Task<OperationStatus> Delete(DataRequest dataRequest)
        {
            return await operations.Delete(dataRequest);
        }

    }
}
