﻿using HoneyWell.OneDAT.CatalystFamily.API.Application.Queries;
using HoneyWell.OneDAT.CatalystFamily.API.Application.Validators;
using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.CatalystFamily.API.Application.Services
{
    public class CatalysFamilyService : OneDATService
    {
        readonly Operations operations;
        readonly BussinessValidator insertValidator;
        OperationStatus objResult = new OperationStatus();
        public CatalysFamilyService(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
            insertValidator = new BussinessValidator(configuration);
        }

        public override async Task<OperationStatus> Create(DataRequest dataRequest)
        {
            try
            {
                var insertResult = await insertValidator.Validate(dataRequest);

                if (!insertResult.status)
                {
                    objResult.Status = "Fail";
                    objResult.Message = insertResult.message.ToString();
                    return objResult;
                }

                return await operations.Create(dataRequest);
            }
            catch (Exception ex)
            {
                objResult.Status = "Fail";
                objResult.Message = "Error :" + ex.ToString();
                return objResult;
            }
        }

        public override async Task<OperationStatus> Read(DataRequest dataRequest)
        {
            try
            {
                Query qry = new Query();
                string sqlQuery = qry.GetQueryFamily(dataRequest);  
                return await operations.Read(dataRequest, sqlQuery);
            }
            catch (Exception ex)
            {
                objResult.Status = "Fail";
                objResult.Message = "Error :" + ex.ToString();
                return objResult;
            }
        }

        public override async Task<OperationStatus> Update(DataRequest dataRequest)
        {
            return await operations.Update(dataRequest);
        }

        public override async Task<OperationStatus> Delete(DataRequest dataRequest)
        {
            return await operations.Delete(dataRequest);
        }

    }

}
