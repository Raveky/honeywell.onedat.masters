﻿using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.Commons.Model;

namespace HoneyWell.OneDAT.CatalystFamily.API.Application.Queries
{
    public class Query : IQuery
    {
        private string _Query;

        public string GetQuery()
        {

            return _Query;
        }

        public string GetQuery(string queryName)
        {
            throw new System.NotImplementedException();
        }

        public string GetQueryApplication()
        {
            _Query = " SELECT CATALYST_APPLICATION_ID_SQ AS CATALYST_APPLICATION_ID_SQ, CATALYST_APPLICATION_NM AS CATALYST_APPLN_NM, CATALYST_TYPE_ID CATALYST_TYPE_ID "+
                    "FROM sc_Catalyst.CATALYST_APPLICATION " +
                    "WHERE ACTIVE_IND = 'Y' " +
                    "ORDER BY CATALYST_APPLICATION_ID_SQ ";

      
            return _Query;
        }

        public string GetQueryFamily(DataRequest dataRequest)
        {
            _Query = "SELECT fpt.CATALYST_FAMILY_ID_SQ,fpt.COMMERCIAL_FAMILY_NM,fpt.PROGRAM_FAMILY_NM,fpt.CATALYST_FAMILY_IND,CASE WHEN fpt.CATALYST_FAMILY_IND = 'C' THEN 'Commercial Name'ELSE 'Program Name' END AS CATALYST_FAMILY_IND_NM,fpt.CATALYST_APPLICATION_ID,fpt.CATALYST_APPLICATION_NM,fpt.CATALYST_TYPE_ID,fpt.CATALYST_TYPE_NM,fpt.PD_AR_MIN_MSR,fpt.PD_AR_MAX_MSR,fpt.LOI_MIN_MSR,fpt.LOI_MAX_MSR,fpt.SULFUR_MIN_MSR,fpt.SULFUR_MAX_MSR,fpt.SDS_NUM,fpt.ACTIVE_IND " +
                    "FROM(SELECT spt.CATALYST_FAMILY_ID_SQ,spt.COMMERCIAL_FAMILY_NM,spt.PROGRAM_FAMILY_NM,spt.CATALYST_FAMILY_IND,spt.CATALYST_APPLICATION_ID,spt.CATALYST_APPLICATION_NM,spt.CATALYST_TYPE_ID,spt.CATALYST_TYPE_NM,spt.PD_AR_MIN_MSR,spt.PD_AR_MAX_MSR,spt.LOI_MIN_MSR,spt.LOI_MAX_MSR,spt.SULFUR_MIN_MSR,spt.SULFUR_MAX_MSR,spt.SDS_NUM,spt.ACTIVE_IND " +
                    "FROM(SELECT A.CATALYST_FAMILY_ID_SQ,A.COMMERCIAL_FAMILY_NM, A.PROGRAM_FAMILY_NM, A.CATALYST_FAMILY_IND, B.CATALYST_APPLICATION_ID_SQ CATALYST_APPLICATION_ID,B.CATALYST_APPLICATION_NM, C.CATALYST_TYPE_ID_SQ CATALYST_TYPE_ID,C.CATALYST_TYPE_NM,A.PD_AR_MIN_MSR, A.PD_AR_MAX_MSR,A.LOI_MIN_MSR, A.LOI_MAX_MSR, A.SULFUR_MIN_MSR, A.SULFUR_MAX_MSR,  A.SDS_NUM,A.ACTIVE_IND " +
                    "FROM sc_catalyst.CATALYST_FAMILY A " +
                    "LEFT JOIN sc_catalyst.CATALYST_APPLICATION B ON A.CATALYST_APPLICATION_ID = B.CATALYST_APPLICATION_ID_SQ " +
                    "JOIN sc_catalyst.CATALYST_TYPE C ON A.CATALYST_TYPE_ID = C.CATALYST_TYPE_ID_SQ )spt )fpt";
            return _Query;
        }
    }
}
