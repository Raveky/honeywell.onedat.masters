﻿using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Infrastructure.Validations;
using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.CatalystFamily.API.Application.Validators
{
    public class BussinessValidator : OneDATService, IValidator
    {
        readonly Operations operations;
        readonly OperationStatus operationStatus;
        readonly CommonValidations commonValidations;

        public BussinessValidator(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
            operationStatus = new OperationStatus();
            commonValidations = new CommonValidations(configuration);
        }

        public async Task<(bool status, string message)> Validate(DataRequest dataRequest)
        {
            if (dataRequest.Messages[0].fields.Count > 0)
            {
                foreach (var data in dataRequest.Messages[0].fields)
                {
                    if (data.field_name == "commercial_family_nm")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Catalyst Family Name");
                        }
                        else if ((!System.Text.RegularExpressions.Regex.IsMatch(data.field_value, @"^\w*([\s-_]*\w*)*$")))
                        {
                            return (false, "Invalid Catalyst Family Code");
                        }
                        bool isDuplicateValue = await commonValidations.ChecksDuplicateRecord(data, dataRequest);

                        if (isDuplicateValue)
                        {
                            return (false, "Duplicate Catalyst Family Code");
                        }
                    }

                    if (data.field_name == "program_family_nm")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Catalyst Family Name");
                        }
                        else if ((!System.Text.RegularExpressions.Regex.IsMatch(data.field_value, @"^\w*([\s-_]*\w*)*$")))
                        {
                            return (false, "Invalid Catalyst Family Name");
                        }
                        bool isDuplicateValue = await commonValidations.ChecksDuplicateRecord(data, dataRequest);

                        if (isDuplicateValue)
                        {
                            return (false, "Duplicate Catalyst FamilyName");
                        }
                    }
                    if (data.field_name == "catalyst_family_desc")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Catalyst Family Description");
                        }
                        else if ((!System.Text.RegularExpressions.Regex.IsMatch(data.field_value, @"^\w*([\s-_.,]*\w*)*$")))
                        {
                            return (false, "Invalid Catalyst Family Description");
                        }
                        bool isDuplicateValue = await commonValidations.ChecksDuplicateRecord(data, dataRequest);

                        if (isDuplicateValue)
                        {
                            return (false, "Duplicate Catalyst Family Description");
                        }
                    }
                   
                    if (data.field_name == "commercial_soak")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Commercial Soak");
                        }
                        else if ((!System.Text.RegularExpressions.Regex.IsMatch(data.field_value, @"^[0-9]+(\.[0-9][0-9]?)?")))
                        {
                            return (false, "Invalid Catalyst Commercial Soak");
                        }
                    }
                    if (data.field_name == "commercial_dense")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Commercial Dense");
                        }
                        else if ((!System.Text.RegularExpressions.Regex.IsMatch(data.field_value, @"^[0-9]+(\.[0-9][0-9]?)?")))
                        {
                            return (false, "Invalid Catalyst Commercial Dense");
                        }
                    }

                    if (data.field_name == "active_ind")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Catalyst Family Status");
                        }
                        else if (!(data.field_value.ToLower() == "y" || data.field_value.ToLower() == "n"))
                        {
                            return (false, "Invalid value of Status");
                        }
                    }
                }
            }
            return (true, "Valid Data");
        }

        public async Task<(bool status, string message)> ValidateRead(DataRequest dataRequest)
        {
            if (dataRequest.Messages[0].filter.Count > 0)
            {
                foreach (var data in dataRequest.Messages[0].filter)
                {
                    if (data.field_name == "commercial_family_nm")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Catalyst Family Name");
                        }
                        else if ((!System.Text.RegularExpressions.Regex.IsMatch(data.field_value, @"^\w*([\s-_]*\w*)*$")))
                        {
                            return (false, "Invalid Catalyst Family Code");
                        }
                     
                    }

                    if (data.field_name == "program_family_nm")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Catalyst Family Name");
                        }
                        else if ((!System.Text.RegularExpressions.Regex.IsMatch(data.field_value, @"^\w*([\s-_]*\w*)*$")))
                        {
                            return (false, "Invalid Catalyst Family Name");
                        }
                       
                    }
                    if (data.field_name == "catalyst_family_desc")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Catalyst Family Description");
                        }
                        else if ((!System.Text.RegularExpressions.Regex.IsMatch(data.field_value, @"^\w*([\s-_.,]*\w*)*$")))
                        {
                            return (false, "Invalid Catalyst Family Description");
                        }
                    }
                   
                    if (data.field_name == "commercial_soak")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Commercial Soak");
                        }
                        else if ((!System.Text.RegularExpressions.Regex.IsMatch(data.field_value, @"^[0-9]+(\.[0-9][0-9]?)?")))
                        {
                            return (false, "Invalid Catalyst Commercial Soak");
                        }
                    }
                    if (data.field_name == "commercial_dense")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Commercial Dense");
                        }
                        else if ((!System.Text.RegularExpressions.Regex.IsMatch(data.field_value, @"^[0-9]+(\.[0-9][0-9]?)?")))
                        {
                            return (false, "Invalid Catalyst Commercial Dense");
                        }
                    }

                    if (data.field_name == "active_ind")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Catalyst Family Status");
                        }
                        else if (!(data.field_value.ToLower() == "y" || data.field_value.ToLower() == "n"))
                        {
                            return (false, "Invalid value of Status");
                        }
                    }
                }
            }
            return (true, "Valid Data");
        }
    }
}

