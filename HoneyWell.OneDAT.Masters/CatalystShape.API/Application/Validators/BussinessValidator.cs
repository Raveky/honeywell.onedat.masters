﻿using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Infrastructure.Validations;
using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.CatalystShape.API.Application.Validators
{
    public class BussinessValidator : OneDATService, IValidator
    {
        readonly Operations operations;
        readonly OperationStatus operationStatus;
        readonly CommonValidations commonValidations;

        public BussinessValidator(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
            operationStatus = new OperationStatus();
            commonValidations = new CommonValidations(configuration);
        }

        public async Task<(bool status, string message)> Validate(DataRequest dataRequest)
        {
            if (dataRequest.Messages[0].fields.Count > 0)
            {
                foreach (var data in dataRequest.Messages[0].fields)
                {
                    if (data.field_name == "catalyst_shape_nm")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Catalyst Shape ");
                        }
                        else if ((!System.Text.RegularExpressions.Regex.IsMatch(data.field_value, @"^\w*([\s-_]*\w*)*$")))
                        {
                            return (false, "Invalid Catalyst Shape");
                        }

                        bool isDuplicateValue = await commonValidations.ChecksDuplicateRecord(data, dataRequest);

                        if (isDuplicateValue)
                        {
                            return (false, "Duplicate Catalyst shape");
                        }
                    }
                    if (data.field_name == "assumed_voidfraction_msr")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Void Fraction");
                        }
                        else if (data.field_value.Length > 10)
                        {
                            return (false, "Fieldlength should not be greather that 10");
                        }

                        try
                        {
                            Convert.ToDecimal(data.field_value);
                        }
                        catch (Exception ex)
                        {
                            return (false, "Field value should be decimal");
                        }
                    }
                    if (data.field_name == "crushed_fraction_msr")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Void Fraction");
                        }
                        else if (data.field_value.Length > 10)
                        {
                            return (false, "Fieldlength should not be greather that 10");
                        }

                        try
                        {
                            Convert.ToDecimal(data.field_value);
                        }
                        catch (Exception ex)
                        {
                            return (false, "Field value should be decimal");
                        }
                    }

                    if (data.field_name == "active_ind")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Catalyst Shape Status");
                        }
                        else if (!(data.field_value.ToLower() == "y" || data.field_value.ToLower() == "n"))
                        {
                            return (false, "Invalid value of Status");
                        }
                    }
                }
            }
            return (true, "Valid Data");
        }

        public async Task<(bool status, string message)> ValidateRead(DataRequest dataRequest)
        {
            if (dataRequest.Messages[0].filter.Count > 0)
            {
                foreach (var data in dataRequest.Messages[0].filter)
                {
                    if (data.field_name == "catalyst_shape_nm")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Catalyst Shape");
                        }
                        else if ((!System.Text.RegularExpressions.Regex.IsMatch(data.field_value, @"^\w*([\s-_]*\w*)*$")))
                        {
                            return (false, "Invalid Catalyst Shape");
                        }
                    }

                    if (data.field_name == "assumed_voidfraction_msr")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Void Fraction");
                        }
                        else if (data.field_value.Length > 10)
                        {
                            return (false, "Fieldlength should not be greather that 10");
                        }

                        try
                        {
                            Convert.ToDecimal(data.field_value);
                        }
                        catch (Exception ex)
                        {
                            return (false, "Field value should be decimal");
                        }
                    }
                    if (data.field_name == "crushed_fraction_msr")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Void Fraction");
                        }
                        else if (data.field_value.Length > 10)
                        {
                            return (false, "Fieldlength should not be greather that 10");
                        }

                        try
                        {
                            Convert.ToDecimal(data.field_value);
                        }
                        catch (Exception ex)
                        {
                            return (false, "Field value should be decimal");
                        }
                    }

                    if (data.field_name == "active_ind")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Catalyst Shape Status");
                        }
                        else if (!(data.field_value.ToLower() == "y" || data.field_value.ToLower() == "n"))
                        {
                            return (false, "Invalid value of Status");
                        }
                    }
                }
            }
            return (true, "Valid Data");
        }
    }
}

